#!/bin/bash
#
#ищем все альтернативы с именами
names=$(sudo update-alternatives --get-selections | grep mpi |\
    awk '{print $1;}'
)
#добавить выбор альтернативы:
names+=() #сюда записать нужные названия
#исключить альтернативу:
exclude=() #сюда записать нужные названия
for del in ${exclude[@]}
do
   names=("${names[@]/$del}") 
done

for name in ${names[*]}
do
    # и изменяем их
    sudo update-alternatives --config $name
done

